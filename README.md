# JW player media source for media library

## Installation and configuration

Install with composer
```
composer require 'drupal/jw_video_media_source:1.0.x-dev@dev'
```

Enable module in Drupal admin or with drush command
```
drush pm-enable jw_video_media_source -y
```

Create media types under Structure >> Media types >> Add media type
Select JW video from Media source
Modify Thumbnail location and Video size if you need.

After save, you can set the display for this media type.
Choose `JW Player content` from format and, you can set the video tag
attributes (Show controls, Loop video, Mute video, Autoplay, Width, Height).
