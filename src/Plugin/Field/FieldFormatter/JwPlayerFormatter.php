<?php

namespace Drupal\jw_video_media_source\Plugin\Field\FieldFormatter;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\jw_video_media_source\MediaFetcher;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'jw player' formatter.
 *
 * @FieldFormatter(
 *   id = "jwplayer",
 *   label = @Translation("JW Player content"),
 *   field_types = {
 *     "link",
 *     "string",
 *     "string_long",
 *   },
 * )
 */
class JwPlayerFormatter extends FormatterBase {

  /**
   * Media fetcher for JW videos.
   *
   * @var \Drupal\jw_video_media_source\MediaFetcher
   */
  protected MediaFetcher $mediaFetcher;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The media settings config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Constructs an JwPlayerFormatter instance.
   *
   * @param string $plugin_id
   *   The plugin ID for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\jw_video_media_source\MediaFetcher $media_fetcher
   *   The Media fetcher service.
   */
  public function __construct($plugin_id,
                              $plugin_definition,
                              FieldDefinitionInterface $field_definition,
                              array $settings,
                              $label,
                              $view_mode,
                              array $third_party_settings,
                              MessengerInterface $messenger,
                              LoggerChannelFactoryInterface $logger_factory,
                              ConfigFactoryInterface $config_factory,
                              MediaFetcher $media_fetcher) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->messenger = $messenger;
    $this->logger = $logger_factory->get('media');
    $this->config = $config_factory->get('media.settings');
    $this->mediaFetcher = $media_fetcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('messenger'),
      $container->get('logger.factory'),
      $container->get('config.factory'),
      $container->get('jw_video_media_source.media_fetcher'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'controls' => FALSE,
      'loop' => FALSE,
      'muted' => FALSE,
      'autoplay' => FALSE,
      'max_width' => 0,
      'max_height' => 0,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {

    $summary = parent::settingsSummary();

    $summary[] = $this->t('Playback controls: %status', ['%status' => $this->getSetting('controls') ? 'visible' : 'hidden']);
    $summary[] = $this->t('Autoplay: %status', ['%status' => $this->getSetting('autoplay') ? 'yes' : 'no']);
    $summary[] = $this->t('Loop %status', ['%status' => $this->getSetting('loop') ? 'yes' : 'no']);
    $summary[] = $this->t('Muted: %status', ['%status' => $this->getSetting('muted') ? 'yes' : 'no']);

    if ($this->getSetting('max_width') && $this->getSetting('max_height')) {
      $summary[] = $this->t('Maximum size: %max_width x %max_height pixels', [
        '%max_width' => $this->getSetting('max_width'),
        '%max_height' => $this->getSetting('max_height'),
      ]);
    }
    elseif ($this->getSetting('max_width')) {
      $summary[] = $this->t('Maximum width: %max_width pixels', [
        '%max_width' => $this->getSetting('max_width'),
      ]);
    }
    elseif ($this->getSetting('max_height')) {
      $summary[] = $this->t('Maximum height: %max_height pixels', [
        '%max_height' => $this->getSetting('max_height'),
      ]);
    }
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    $max_width = $this->getSetting('max_width');
    $max_height = $this->getSetting('max_height');

    foreach ($items as $delta => $item) {
      $main_property = $item->getFieldDefinition()->getFieldStorageDefinition()->getMainPropertyName();
      $jw_media_id = $item->{$main_property};

      if (empty($jw_media_id)) {
        continue;
      }

      $media_type_id = $item->getFieldDefinition()->get('bundle');
      try {
        $resource = $this->mediaFetcher->fetch($jw_media_id, $media_type_id);
      }
      catch (\Exception $e) {
        $this->logger->error("Could not fetch jw data: " . $e->getMessage());
        continue;
      }

      $element[$delta] = [
        '#type' => 'html_tag',
        '#tag' => 'video',
        '#attributes' => [
          'width' => $max_width ?: $resource->getWidth(),
          'height' => $max_height ?: $resource->getHeight(),
          'class' => ['jw-player-content'],
        ],
        'child' => [
          '#type' => 'html_tag',
          '#tag' => 'source',
          '#attributes' => [
            'src' => $resource->getUrl(),
            'type' => $resource->getType(),
          ],
        ],
        '#attached' => [
          'library' => [
            'jw_video_media_source/jw-player.formatter',
          ],
        ],
      ];

      foreach (['controls', 'loop', 'autoplay', 'muted'] as $key) {
        if ($this->getSetting($key)) {
          $element[$delta]['#attributes'][$key] = TRUE;
        }
      }

      $title = $resource->getTitle();
      if ($title) {
        $element[$delta]['#attributes']['title'] = $title;
      }

      CacheableMetadata::createFromObject($resource)
        ->addCacheTags($this->config->getCacheTags())
        ->applyTo($element[$delta]);

    }
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return parent::settingsForm($form, $form_state) + [
      'controls' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Show controls'),
        '#default_value' => $this->getSetting('controls'),
      ],
      'loop' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Loop video'),
        '#default_value' => $this->getSetting('loop'),
      ],
      'muted' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Mute video'),
        '#default_value' => $this->getSetting('muted'),
      ],
      'autoplay' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Autoplay'),
        '#default_value' => $this->getSetting('autoplay'),
      ],
      'max_width' => [
        '#type' => 'number',
        '#title' => $this->t('Maximum width'),
        '#default_value' => $this->getSetting('max_width'),
        '#size' => 5,
        '#maxlength' => 5,
        '#field_suffix' => $this->t('pixels'),
        '#min' => 0,
      ],
      'max_height' => [
        '#type' => 'number',
        '#title' => $this->t('Maximum height'),
        '#default_value' => $this->getSetting('max_height'),
        '#size' => 5,
        '#maxlength' => 5,
        '#field_suffix' => $this->t('pixels'),
        '#min' => 0,
      ],
    ];
  }

}
