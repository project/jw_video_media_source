<?php

namespace Drupal\jw_video_media_source\Plugin\media\Source;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\media\MediaInterface;
use Drupal\media\MediaSourceBase;
use Drupal\jw_video_media_source\MediaFetcher;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a media source plugin for JW videos.
 *
 * @MediaSource(
 *   id = "jw_video",
 *   label = @Translation("JW video"),
 *   description = @Translation("Use jw videos."),
 *   allowed_field_types = {"string"},
 *   thumbnail_alt_metadata_attribute = "alt",
 *   default_thumbnail_filename = "no-thumbnail.png",
 *   forms = {
 *     "media_library_add" = "Drupal\jw_video_media_source\Form\JwPlayerForm"
 *   }
 * )
 */
class JwVideo extends MediaSourceBase {

  /**
   * Media fetcher for JW videos.
   *
   * @var \Drupal\jw_video_media_source\MediaFetcher
   */
  protected MediaFetcher $mediaFetcher;

  /**
   * Constructs a new JwVideo instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\Field\FieldTypePluginManagerInterface $field_type_manager
   *   The field type plugin manager service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\jw_video_media_source\MediaFetcher $media_fetcher
   *   The media fetcher service.
   */
  public function __construct(array $configuration,
                              $plugin_id,
                              $plugin_definition,
                              EntityTypeManagerInterface $entity_type_manager,
                              EntityFieldManagerInterface $entity_field_manager,
                              ConfigFactoryInterface $config_factory,
                              FieldTypePluginManagerInterface $field_type_manager,
                              MessengerInterface $messenger,
                              MediaFetcher $media_fetcher
                              ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $entity_field_manager, $field_type_manager, $config_factory);
    $this->messenger = $messenger;
    $this->mediaFetcher = $media_fetcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('config.factory'),
      $container->get('plugin.manager.field.field_type'),
      $container->get('messenger'),
      $container->get('jw_video_media_source.media_fetcher'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadataAttributes() {
    return [
      'type' => $this->t('Type'),
      'title' => $this->t('Title'),
      'name' => $this->t('Name'),
      'default_name' => $this->t('Default name'),
      'description' => $this->t('Description'),
      'media_id' => $this->t('JW media ID'),
      'duration' => $this->t('Duration'),
      'thumbnail_uri' => $this->t('Local URI of the thumbnail'),
      'thumbnail_width' => $this->t('Thumbnail width'),
      'thumbnail_height' => $this->t('Thumbnail height'),
      'url' => $this->t('The URL'),
      'width' => $this->t('Video width'),
      'height' => $this->t('Video height'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadata(MediaInterface $media, $name) {
    $media_id = $media->get($this->configuration['source_field'])->value;

    try {
      $media_type_id = $media->bundle();
      $resource = $this->mediaFetcher->fetch($media_id, $media_type_id);
    }
    catch (\Exception $e) {
      $this->messenger->addError($e->getMessage());
      return NULL;
    }

    switch ($name) {
      case 'default_name':
        if ($title = $this->getMetadata($media, 'title')) {
          return $title;
        }

        if ($url = $this->getMetadata($media, 'url')) {
          return $url;
        }
        return parent::getMetadata($media, 'default_name');

      case 'thumbnail_uri':
        return $resource->getThumbnailUri();

      case 'type':
        return $resource->getType();

      case 'title':
        return $resource->getTitle();

      case 'description':
        return $resource->getDescription();

      case 'duration':
        return $resource->getDuration();

      case 'media_id':
        return $resource->getMediaId();

      case 'url':
        return $resource->getUrl();

      case 'width':
        return $resource->getWidth();

      case 'height':
        return $resource->getHeight();

      case 'thumbnail_width':
        return $resource->getThumbnailWidth();

      case 'thumbnail_height':
        return $resource->getThumbnailHeight();

      default:
        break;
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'thumbnails_directory' => 'public://jw_thumbnails',
      'video_size' => 1280,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildConfigurationForm($form, $form_state);

    $form['thumbnails_directory'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Thumbnails location'),
      '#default_value' => $this->configuration['thumbnails_directory'],
      '#description' => $this->t('Thumbnails will be fetched from the <a href="@api_url">JW Player API</a> for local usage. This is the URI of the directory where they will be placed.',
      [':api_url' => 'https://developer.jwplayer.com/jwplayer/reference/get_v2-media-media-id']),
      '#required' => TRUE,
    ];

    $form['video_size'] = [
      '#type' => 'select',
      '#title' => $this->t('Video size'),
      '#default_value' => $this->configuration['video_size'],
      '#options' => [
        1280 => $this->t('1280x720'),
        640 => $this->t('640x360'),
        448 => $this->t('448x252'),
      ],
      '#required' => TRUE,
    ];

    return $form;
  }

}
