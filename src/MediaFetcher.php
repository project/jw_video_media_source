<?php

namespace Drupal\jw_video_media_source;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Crypt;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\UseCacheBackendTrait;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\File\Exception\FileException;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\TransferException;
use Symfony\Component\Mime\MimeTypes;

/**
 * Fetches and caches oEmbed resources.
 */
class MediaFetcher {

  use UseCacheBackendTrait;

  /**
   * The logger channel for media.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected FileSystemInterface $fileSystem;

  /**
   * EntityTypeManager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected EntityTypeManager $entityTypeManager;

  /**
   * Url.
   *
   * @var string
   */
  protected string $url = '';

  /**
   * Video width.
   *
   * @var int
   */
  protected int $width;

  /**
   * Video width.
   *
   * @var int
   */
  protected int $height;

  /**
   * Video type.
   *
   * @var string
   */
  protected string $type;

  /**
   * Video title.
   *
   * @var string
   */
  protected string $title;

  /**
   * Video description.
   *
   * @var string
   */
  protected string $description;

  /**
   * JW API media id.
   *
   * @var string
   */
  protected string $mediaId;

  /**
   * Video duration.
   *
   * @var int
   */
  protected int $duration;

  /**
   * Thumbnail local uri.
   *
   * @var string
   */
  protected string $thumbnailUri;

  /**
   * Thumbnail width.
   *
   * @var int
   */
  protected int $thumbnailWidth;

  /**
   * Thumbnail height.
   *
   * @var int
   */
  protected int $thumbnailHeight;

  /**
   * Constructs a MediaFetcher object.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Cache\CacheBackendInterface|null $cache_backend
   *   (optional) The cache backend.
   */
  public function __construct(
    ClientInterface $http_client,
    FileSystemInterface $file_system,
    LoggerChannelFactoryInterface $logger_factory,
    EntityTypeManager $entity_type_manager,
    CacheBackendInterface $cache_backend = NULL) {
    $this->httpClient = $http_client;
    $this->cacheBackend = $cache_backend;
    $this->useCaches = isset($cache_backend);
    $this->fileSystem = $file_system;
    $this->logger = $logger_factory->get('media');
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Fetch data from JW API.
   *
   * @param string $media_id
   *   JW API media id.
   * @param string $media_type_id
   *   Media type id.
   *
   * @return $this
   *   MediaFetcher.
   *
   * @throws \Exception
   */
  public function fetch(string $media_id, string $media_type_id): MediaFetcher {

    $media_type = $this->entityTypeManager
      ->getStorage('media_type')
      ->load($media_type_id);
    if ($media_type !== NULL) {
      $config = $media_type->getSource()->getConfiguration();
    }
    else {
      throw new \Exception('Media type does not exists: ' . $media_type_id);
    }

    $cache_id = "media:jw_player_resource:$media_id";

    $cached = $this->cacheGet($cache_id);

    if ($cached) {
      return $this->createResource($cached->data, $config);
    }

    try {
      $response = $this->httpClient->get('https://cdn.jwplayer.com/v2/media/' . $media_id, [
        'Accept' => 'application/json; charset=utf-8',
      ]);
    }
    catch (ClientException $e) {
      throw new \Exception('Could not retrieve JW player media ID. ' . $e->getMessage());
    }

    $content = (string) $response->getBody();
    $data = Json::decode($content);

    if (json_last_error() !== JSON_ERROR_NONE) {
      throw new \Exception('Error decoding JW player resource: ' . json_last_error_msg());
    }

    if (empty($data) || !is_array($data)) {
      throw new \Exception('The JW player resource could not be decoded.');
    }

    $this->cacheSet($cache_id, $data);

    return $this->createResource($data, $config);
  }

  /**
   * Populate instance properties with data.
   *
   * @param array $data
   *   Data from JW API.
   * @param array $config
   *   Media type config array.
   *
   * @return $this
   *   MediaFetcher object.
   */
  protected function createResource(array $data, array $config): MediaFetcher {

    $this->title = $data['title'];
    $this->description = $data['description'];
    $this->mediaId = $data['playlist'][0]['mediaid'];
    $this->duration = $data['playlist'][0]['duration'];

    $this->thumbnailUri = $this->getLocalThumbnailUri($data['playlist'][0]['image'], $config['thumbnails_directory']);

    foreach ($data['playlist'][0]['sources'] as $item) {
      if ($item['type'] === 'video/mp4') {
        $this->type = 'video/mp4';
        $this->url = $item['file'];
        $this->width = $item['width'];
        $this->height = $item['height'];
        if ($item['width'] === (int) $config['video_size']) {
          break;
        }
      }
    }
    return $this;
  }

  /**
   * Save JW API image to local directory and return its uri.
   *
   * @param string $remote_thumbnail_url
   *   Remote url.
   * @param string $directory
   *   Thumbnail directory.
   *
   * @return string|null
   *   Local thumbnail uri.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   *   Exception.
   */
  protected function getLocalThumbnailUri(string $remote_thumbnail_url, string $directory): ?string {

    // The local thumbnail doesn't exist yet, so try to download it. First,
    // ensure that the destination directory is writable, and if it's not,
    // log an error and bail out.
    if (!$this->fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS)) {
      $this->logger->warning('Could not prepare thumbnail destination directory @dir for JW video media.', [
        '@dir' => $directory,
      ]);
      return NULL;
    }

    // The local filename of the thumbnail is always a hash of its remote URL.
    // If a file with that name already exists in the thumbnails directory,
    // regardless of its extension, return its URI.
    $hash = Crypt::hashBase64($remote_thumbnail_url);
    $files = $this->fileSystem->scanDirectory($directory, "/^$hash\..*/");
    if (count($files) > 0) {
      return reset($files)->uri;
    }

    // The local thumbnail doesn't exist yet, so we need to download it.
    $local_thumbnail_uri = $directory . DIRECTORY_SEPARATOR . $hash . '.' . $this->getThumbnailFileExtensionFromUrl($remote_thumbnail_url);
    try {
      $response = $this->httpClient->request('GET', $remote_thumbnail_url);
      if ($response->getStatusCode() === 200) {
        $this->fileSystem->saveData((string) $response->getBody(), $local_thumbnail_uri, FileSystemInterface::EXISTS_REPLACE);
        return $local_thumbnail_uri;
      }
    }
    catch (TransferException $e) {
      $this->logger->warning($e->getMessage());
    }
    catch (FileException $e) {
      $this->logger->warning('Could not download remote thumbnail from {url}.', [
        'url' => $remote_thumbnail_url,
      ]);
    }
    return NULL;
  }

  /**
   * Tries to determine the file extension of a thumbnail.
   *
   * @param string $thumbnail_url
   *   The remote URL of the thumbnail.
   *
   * @return string|null
   *   The file extension, or NULL if it could not be determined.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected function getThumbnailFileExtensionFromUrl(string $thumbnail_url): ?string {
    // First, try to glean the extension from the URL path.
    $path = parse_url($thumbnail_url, PHP_URL_PATH);
    if ($path) {
      $extension = strtolower(pathinfo($path, PATHINFO_EXTENSION));
      if ($extension) {
        return $extension;
      }
    }

    // If the URL didn't give us any clues about the file extension, make a HEAD
    // request to the thumbnail URL and see if the headers will give us a MIME
    // type.
    try {
      $content_type = $this->httpClient->request('HEAD', $thumbnail_url)
        ->getHeader('Content-Type');
    }
    catch (TransferException $e) {
      $this->logger->warning($e->getMessage());
    }

    // If there was no Content-Type header, there's nothing else we can do.
    if (empty($content_type)) {
      return NULL;
    }
    $extensions = MimeTypes::getDefault()->getExtensions(reset($content_type));
    if ($extensions) {
      return reset($extensions);
    }
    // If no file extension could be determined from the Content-Type header,
    // we're stumped.
    return NULL;
  }

  /**
   * Returns the url for the JW video.
   *
   * @return string
   *   Url.
   */
  public function getUrl(): string {
    return $this->url;
  }

  /**
   * Returns the width of the JW video.
   *
   * @return int
   *   JW video width.
   */
  public function getWidth(): int {
    return $this->width;
  }

  /**
   * Returns the height of the JW video.
   *
   * @return int
   *   JW video width.
   */
  public function getHeight(): int {
    return $this->height;
  }

  /**
   * Returns the type of the JW video.
   *
   * @return string
   *   JW video type.
   */
  public function getType(): string {
    return $this->type;
  }

  /**
   * Returns the title of the JW video.
   *
   * @return string
   *   JW video title.
   */
  public function getTitle(): string {
    return $this->title;
  }

  /**
   * Returns the description of the JW video.
   *
   * @return string
   *   JW video description.
   */
  public function getDescription(): string {
    return $this->description;
  }

  /**
   * Returns the media id of the JW video.
   *
   * @return string
   *   JW video media id.
   */
  public function getMediaId(): string {
    return $this->media_id;
  }

  /**
   * Returns the duration of the JW video.
   *
   * @return int
   *   JW video duration.
   */
  public function getDuration(): int {
    return $this->duration;
  }

  /**
   * Returns the local thumbnail uri of the JW video.
   *
   * @return string
   *   JW video local thumbnail.
   */
  public function getThumbnailUri(): string {
    return $this->thumbnailUri;
  }

  /**
   * Returns the local thumbnail width of the JW video.
   *
   * @return int
   *   JW video local thumbnail width.
   */
  public function getThumbnailWidth(): int {
    return $this->thumbnailWidth;
  }

  /**
   * Returns the local thumbnail height of the JW video.
   *
   * @return int
   *   JW video local thumbnail height.
   */
  public function getThumbnailHeight(): int {
    return $this->thumbnailHeight;
  }

}
