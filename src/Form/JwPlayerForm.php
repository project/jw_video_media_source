<?php

namespace Drupal\jw_video_media_source\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\media_library\Form\AddFormBase;
use Drupal\media_library\MediaLibraryUiBuilder;
use Drupal\media_library\OpenerResolverInterface;
use Drupal\jw_video_media_source\MediaFetcher;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Creates a form to create media entities from JW player media_id.
 *
 * @internal
 *   Form classes are internal.
 */
class JwPlayerForm extends AddFormBase {

  /**
   * Media fetcher for JW videos.
   *
   * @var \Drupal\jw_video_media_source\MediaFetcher
   */
  protected MediaFetcher $mediaFetcher;

  /**
   * Constructs a new OEmbedForm.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\media_library\MediaLibraryUiBuilder $library_ui_builder
   *   The media library UI builder.
   * @param \Drupal\media\OEmbed\UrlResolverInterface $url_resolver
   *   The oEmbed URL resolver service.
   * @param \Drupal\media\OEmbed\ResourceFetcherInterface $resource_fetcher
   *   The oEmbed resource fetcher service.
   * @param \Drupal\media_library\OpenerResolverInterface $opener_resolver
   *   The opener resolver.
   */

  /**
   * Constructs a new JwPlayerForm.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\media_library\MediaLibraryUiBuilder $library_ui_builder
   *   The media library UI builder.
   * @param \Drupal\jw_video_media_source\MediaFetcher $media_fetcher
   *   The Media fetcher service.
   * @param \Drupal\media_library\OpenerResolverInterface|null $opener_resolver
   *   The opener resolver.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, MediaLibraryUiBuilder $library_ui_builder, MediaFetcher $media_fetcher, OpenerResolverInterface $opener_resolver = NULL) {
    parent::__construct($entity_type_manager, $library_ui_builder, $opener_resolver);
    $this->mediaFetcher = $media_fetcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('media_library.ui_builder'),
      $container->get('jw_video_media_source.media_fetcher'),
      $container->get('media_library.opener_resolver')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return $this->getBaseFormId() . '_jw_player';
  }

  /**
   * {@inheritdoc}
   */
  protected function buildInputElement(array $form, FormStateInterface $form_state) {

    // Add a container to group the input elements for styling purposes.
    $form['container'] = [
      '#type' => 'container',
    ];

    $form['container']['media_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Add JW player media ID'),
      '#description' => $this->t('JW media ID.'),
      '#required' => TRUE,
      '#attributes' => [
        'placeholder' => 'Ny05CEfj',
      ],
    ];

    $form['container']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add'),
      '#button_type' => 'primary',
      '#validate' => ['::validateMediaId'],
      '#submit' => ['::addButtonSubmit'],
      '#ajax' => [
        'callback' => '::updateFormCallback',
        'wrapper' => 'media-library-wrapper',
        'url' => Url::fromRoute('media_library.ui'),
        'options' => [
          'query' => $this->getMediaLibraryState($form_state)->all() + [
            FormBuilderInterface::AJAX_FORM_REQUEST => TRUE,
          ],
        ],
      ],
    ];
    return $form;
  }

  /**
   * Validates the Media ID.
   *
   * @param array $form
   *   The complete form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   */
  public function validateMediaId(array &$form, FormStateInterface $form_state) {
    $media_id = $form_state->getValue('media_id');
    $media_type = $this->getMediaType($form_state);

    if ($media_id) {
      try {
        $resource = $this->mediaFetcher->fetch($media_id, $media_type->id());

        if (!$resource->getUrl()) {
          $form_state->setErrorByName('media_id', $this->t('Media Id can not fetch.'));
        }
      }
      catch (\Exception $e) {
        $form_state->setErrorByName('media_id', $e->getMessage());
      }
    }
  }

  /**
   * Submit handler for the add button.
   *
   * @param array $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function addButtonSubmit(array $form, FormStateInterface $form_state) {
    $this->processInputValues([$form_state->getValue('media_id')], $form, $form_state);
  }

}
